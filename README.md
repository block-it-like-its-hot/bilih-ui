Cryptochicks Hackathon

# Give.Kindly UI & Web3JS Implementation

## Pre-requisites
1. Testrpc with minimum 10 accounts running
2. Install static-server npm module if you do not have server to host this html. 
    npm install -g static-server
3. Start the application - > static-server
4. Need to have deployed smart contracts (registration and donation) on the testrpc network already. 
5. Update the new contract address in index.html under RegistrationContractAddress & DonationContractAddress
6. We use metamask to confirm you are authenticated to the network. Before you login please import the account 0 in testrpc into metamask 

## Application Architecture
Its a single page application with basic HTML, CSS & Jquery. Two smart contracts for registering the actors on the network and for tracking donations. 

Below are list of steps for donor flow :
1. Login
2. Creation of a new donation
3. View the transaction receipt
4. Dashboard to view all the donations and check the status of donation. 

All the data flows through blockchain. We dont maintain any centralized servers with data. 

Application would run on port 9080. 